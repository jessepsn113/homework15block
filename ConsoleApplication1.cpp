#include <iostream>
using namespace std;

void Print_num(int a, int b)
{
	for (int c = b; c <= a; c += 2)
		cout << c << " ";
	cout << "\n";
}

int main() 
{
	int a;
	cout << "N="; cin >> a;

	cout << "Chetnie:\n";
	Print_num(a, 0);

	std::cout << "NeChetnie:\n";
	Print_num(a, 1);

	return 0;
	

}
